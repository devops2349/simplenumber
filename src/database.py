from time import time
from src.models import RequestModel


class Database:
    async def save_request(self, number: int, result: bool) -> int:
        """ Сохраняет запрос в БД> """
        return (
            await RequestModel(
                number=str(number),
                result=result,
                timestamp=time()
            ).create()
        ).id

    async def get_requests(self, offset: int, limit: int):
        """ Возвращает записи из БД """
        return [request.__values__ for request in await RequestModel.get_requests(offset, limit)]

    async def delete_requests(self, id_record: int):
        """ Удаляет запись из БД """
        return (await RequestModel.delete_requests(id_record),
                f'Запись с id: {id_record} удалена')

    async def edit_requests(self, id_record: int, new_result: bool):
        """ Редактирует значение поля """
        return (await RequestModel.edit_requests(id_record, new_result),
                f'Запись с id: {id_record} обновлена')
