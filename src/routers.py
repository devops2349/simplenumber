from fastapi import APIRouter
from src.check_simplenumber_service import service


router = APIRouter()


@router.post('/')
async def check_simplenumber(number: int) -> bool:
    return await service.check_simplenumber(number)

@router.get('/requests/')
async def get_requests(offset: int, limit: int):
    return await service.get_requests(offset, limit)

@router.post('/delete_requests/')
async def delete_requests(id_record: int):
    return await service.delete_requests(id_record)

@router.post('/edit_requests/')
async def edit_requests(id_record: int, new_result: bool):
    return await service.edit_requests(id_record, new_result)

