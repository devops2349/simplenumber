from logging import getLogger
from logging import FileHandler, StreamHandler, DEBUG, INFO, Formatter
from src.config import log_file


def get_logger(is_debug=False):
    """ Отдает логгер """
    log = getLogger("check_simplenumber")
    file_handler = FileHandler(log_file)
    file_handler.setFormatter(Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    log.addHandler(file_handler)
    log.addHandler(StreamHandler())
    log.setLevel(DEBUG if is_debug else INFO)
    return log
