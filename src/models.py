from gino import Gino
from sqlalchemy import Column, BigInteger, Boolean, String, Float
db = Gino()


async def init_db(database: str, dsn: str):
    """ Подключение Gino к циклу событий """
    await db.set_bind(f"{database}://{dsn}")


class RequestModel(db.Model):
    __tablename__ = 'request'
    id = Column(BigInteger, primary_key=True)
    number = Column(String)  # в БД записываем все отправляемые значения
    result = Column(Boolean)
    timestamp = Column(Float)

    @classmethod
    async def get_requests(cls, offset: int, limit: int):
        return await cls.query.offset(offset).limit(limit).gino.all()

    @classmethod
    async def delete_requests(cls, id_record: int):
        return await cls.delete.where(cls.id == id_record).gino.status()

    @classmethod
    async def edit_requests(cls, id_record: int, new_result: bool):
        return await cls.update.values(result=new_result).where(cls.id == id_record).gino.status()
