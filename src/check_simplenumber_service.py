from src.database import Database
from sympy import *
from src.logger import get_logger


class Service:
    def __init__(self):
        self.database = Database()
        self.log = get_logger()

    async def check_simplenumber(self, number: int) -> bool:
        """ Возвращает True, если проверяемое число простое, и False, если число не простое """
        result = isprime(number)
        if result == True:
            self.log.info(f'Число {number} является простым ')
        else:
            self.log.info(f'Число {number} не простое ')
        await self.database.save_request(number, result)
        return result

    async def get_requests(self, offset: int, limit: int):
        """ Возвращает записи из БД c 'offset' о 'limit' """
        return await self.database.get_requests(offset, limit)

    async def delete_requests(self, id_record: int):
        """ Удаляет запись из БД по 'id_record' """
        self.log.info(f'Удаление записи из БД с id {id_record}')
        return await self.database.delete_requests(id_record)

    async def edit_requests(self, id_record: int, new_result: bool):
        """ Редактирует значение поля 'result' по 'id_record' """
        self.log.info(f'Редактирование записи из БД с id {id_record}')
        return await self.database.edit_requests(id_record, new_result)


service = Service()
