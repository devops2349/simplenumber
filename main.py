import fastapi
import uvicorn
from src.routers import router
from src.models import init_db
from src import config
from logging import getLogger


async def startup():
    await init_db(config.database, config.dsn)


def main():
    app = fastapi.FastAPI()
    app.add_event_handler('startup', startup)
    app.include_router(router)
    uvicorn.run(app, host=config.host, port=config.port)


if __name__ == '__main__':
    main()
